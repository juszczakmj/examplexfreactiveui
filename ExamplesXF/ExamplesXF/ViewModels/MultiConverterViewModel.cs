using System.ComponentModel;
using System.Runtime.CompilerServices;
namespace ExamplesXF.ViewModels
{
    public class MultiConverterViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _redValue;
        public int RedValue
        {
            get => _redValue;
            set
            {
                _redValue = value;
                OnPropertyChanged(nameof(RedValue));
            }
        }
        
        private int _greenValue;
        public int GreenValue
        {
            get => _greenValue;
            set
            {
                _greenValue = value;
                OnPropertyChanged(nameof(GreenValue));
            }
        }
        
        private int _blueValue;
        public int BlueValue
        {
            get => _blueValue;
            set
            {
                _blueValue = value;
                OnPropertyChanged(nameof(BlueValue));
            }
        }

        public MultiConverterViewModel()
        {
            _redValue = _blueValue = _greenValue = 0;
        }
        
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}