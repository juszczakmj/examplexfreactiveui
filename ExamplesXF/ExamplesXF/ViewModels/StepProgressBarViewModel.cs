using System;
using ExamplesXF.Lib.Enums;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace ExamplesXF.ViewModels
{
    public class StepProgressBarViewModel : ReactiveObject
    {
        [Reactive] public int IndicatorNumberOfFields { get; set; }
        [Reactive] public int IndicatorPercentOfProgress { get; set; }
        [Reactive] public bool NoneChecked { get; set; }
        [Reactive] public bool SuccessChecked { get; set; }
        [Reactive] public bool ActiveChecked { get; set; }
        [Reactive] public bool FailedChecked { get; set; }
        [Reactive] public ProgressState StatusOfProgress { get; set; }

        public StepProgressBarViewModel()
        {
            this.WhenAnyValue(
                    x => x.NoneChecked,
                    x => x.SuccessChecked,
                    x => x.ActiveChecked,
                    x => x.FailedChecked)
                .Subscribe(x => StatusOfProgress = UpdateStatusOfProgress());
        }
        
        private ProgressState UpdateStatusOfProgress()
            => (NoneChecked, SuccessChecked, ActiveChecked, FailedChecked) switch
            {
                (true, false, false, false) => ProgressState.None,
                (false, true, false, false) => ProgressState.Success,
                (false, false, true, false) => ProgressState.Active,
                (false, false, false, true) => ProgressState.Failed,
                _ => ProgressState.None
            };
    }
}
