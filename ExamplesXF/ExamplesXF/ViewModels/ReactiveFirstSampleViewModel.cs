using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ReactiveUI;

namespace ExamplesXF.ViewModels
{
    public class ReactiveFirstSampleViewModel : ReactiveObject
    {
        private int _counter;
        private string _dividedBy;
        private IDisposable _counterObservable;

        public ReactiveFirstSampleViewModel()
        {
            var canDecrement = this.WhenAnyValue(x => x.Counter, CheckIfCanDecrement);

            IncrementValueCommand = ReactiveCommand.CreateFromTask(IncrementValueAsync);
            DecrementingValueCommand = ReactiveCommand.Create(() => Counter--, canDecrement);
            
            _counterObservable = this
                .WhenAnyValue(x => x.Counter)
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(ShowLabelIfNeeded);
        }

        private bool CheckIfCanDecrement(int counter)
        {
            return counter > 0;
        }

        public ICommand IncrementValueCommand { get; }
        public ICommand DecrementingValueCommand { get; }

        public int Counter
        {
            get => _counter;
            set => this.RaiseAndSetIfChanged(ref _counter, value);
        }
        
        public string DevidedBy
        {
            get => _dividedBy;
            set => this.RaiseAndSetIfChanged(ref _dividedBy, value);
        }

        private async Task IncrementValueAsync()
        {
            RxApp.MainThreadScheduler.Schedule(() => Counter++);
        }

        private void ShowLabelIfNeeded(int number)
        {
            DevidedBy = number % 5 == 0 ? $"Number DEVIDED by 5" : $"Number NOT DEVIDED by 5";
        }
    }
}