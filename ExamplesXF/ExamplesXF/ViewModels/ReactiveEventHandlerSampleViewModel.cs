using System;
using System.Reactive.Linq;
using Colors.Services;
using ReactiveUI;

namespace ExamplesXF.ViewModels
{
    public class ReactiveEventHandlerSampleViewModel : ReactiveObject
    {
        private int _seconds;
        private int _miliSeconds;
        
        public ReactiveEventHandlerSampleViewModel()
        {
            //handler
            // TODO !!!
            var alert = new EventAlert();
            TimeAlertObservable = Observable.FromEventPattern<AlertEventsArgs>(
                    h => alert.SomeAlert += h,
                    h => alert.SomeAlert -= h)
                .Select(s => TimeSpan.FromMilliseconds(s.EventArgs.NextAlertTime));

            TimeAlertObservable.Subscribe(GetNextTime);
        }

        public int Seconds
        {
            get => _seconds;
            set => this.RaiseAndSetIfChanged(ref _seconds, value);
        }
        
        public int Miliseconds
        {
            get => _miliSeconds;
            set => this.RaiseAndSetIfChanged(ref _miliSeconds, value);
        }
        
        private void GetNextTime(TimeSpan timeSpan)
        {
            Seconds = timeSpan.Seconds;
            Miliseconds = timeSpan.Milliseconds;
        }

        public IObservable<TimeSpan> TimeAlertObservable { get; }
    }
}