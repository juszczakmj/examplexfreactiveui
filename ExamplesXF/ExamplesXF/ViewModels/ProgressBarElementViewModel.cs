using System;
using ExamplesXF.Lib.Enums;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace ExamplesXF.ViewModels
{
    public class ProgressBarElementViewModel : ReactiveObject
    {
        [Reactive] public bool NoneChecked { get; set; }
        [Reactive] public bool SuccessChecked { get; set; }
        [Reactive] public bool ActiveChecked { get; set; }
        [Reactive] public bool FailedChecked { get; set; }
        [Reactive] public ProgressState StatusField { get; set; }

        public ProgressBarElementViewModel()
        {   
            this.WhenAnyValue(
                    x => x.NoneChecked,
                    x => x.SuccessChecked,
                    x => x.ActiveChecked,
                    x => x.FailedChecked)
                .Subscribe(x => StatusField = UpdateStatusField());
        }

        private ProgressState UpdateStatusField()
            => (NoneChecked, SuccessChecked, ActiveChecked, FailedChecked) switch
            {
                (true, false, false, false) => ProgressState.None,
                (false, true, false, false) => ProgressState.Success,
                (false, false, true, false) => ProgressState.Active,
                (false, false, false, true) => ProgressState.Failed,
                _ => ProgressState.None
            };
    }
}
