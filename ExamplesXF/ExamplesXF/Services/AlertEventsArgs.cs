using System;

namespace Colors.Services
{
    // TODO - NOT USED 
    public class AlertEventsArgs : EventArgs
    {
        public int NextAlertTime { get; set; }

        public AlertEventsArgs(int nextAlertTime)
        {
            NextAlertTime = nextAlertTime;
        }
    }
}