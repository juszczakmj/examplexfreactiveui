using System;
using System.Threading.Tasks;

namespace Colors.Services
{
    public class EventAlert
    {
        public EventAlert()
        {
            Task.Run(StartAlertSource);
        }

        public event EventHandler<AlertEventsArgs> SomeAlert = delegate(object sender, AlertEventsArgs args) { };

        private async Task StartAlertSource()
        {
            var rnd = new Random();

            while (true)
            {
                var alertPeriod = rnd.Next(2000, 10000);

                SomeAlert(this, new AlertEventsArgs(alertPeriod));
                await Task.Delay(alertPeriod);
            }
        }
    }
}