namespace ExamplesXF.Lib.Enums
{
    public enum ProgressState
    {
        None,
        Active,
        Success,
        Failed
    }
}
