using System;
using System.Threading;
using System.Threading.Tasks;
using ExamplesXF.Lib.Enums;
using ReactiveUI;
using Xamarin.Forms;

namespace ExamplesXF.Lib.UI.Controls
{
    public partial class ProgressBarElement
    {
        private Task? _animationTask;
        private CancellationTokenSource? _cancellationTokenSource;

        public ProgressBarElement()
        {
            InitializeComponent();

            this.WhenAnyValue(x => x.Status).Subscribe(SetVisualState);

            UpdateFieldAnimation(ProgressState.None);
        }

        public static readonly BindableProperty StatusProperty = BindableProperty.Create(
            nameof(Status),
            typeof(ProgressState),
            typeof(StepProgressBarControl),
            ProgressState.None,
            propertyChanged: OnStatusChanged);

        public ProgressState Status
        {
            get => (ProgressState) GetValue(StatusProperty);
            set => SetValue(StatusProperty, value);
        }

        private static void OnStatusChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            if (bindable is ProgressBarElement element && newvalue is ProgressState newNumber && newvalue != oldvalue)
            {
                element.UpdateFieldAnimation(newNumber);
            }
        }

        private void UpdateFieldAnimation(ProgressState status)
        {
            if (status != ProgressState.Active)
            {
                _cancellationTokenSource?.Cancel();
            }
            else if (_animationTask == null || _animationTask.IsCompleted)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                _animationTask = Task.Run(() => RunFieldAnimationAsync(_cancellationTokenSource.Token));
            }
        }

        private async Task RunFieldAnimationAsync(CancellationToken cancellationToken)
        {
            uint duration = 400;

            while (!cancellationToken.IsCancellationRequested)
            {
                await this.FadeTo(0, duration);
                await this.FadeTo(1, duration);
            }
        }
        
        private void SetVisualState(ProgressState state)
            => VisualStateManager.GoToState(this, state.ToString());
    }
}
