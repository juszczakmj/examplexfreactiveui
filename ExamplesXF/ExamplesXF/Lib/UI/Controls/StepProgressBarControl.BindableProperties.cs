using ExamplesXF.Lib.Enums;
using Xamarin.Forms;

namespace ExamplesXF.Lib.UI.Controls
{
    public partial class StepProgressBarControl
    {
        public static readonly BindableProperty NumberOfFieldsProperty = BindableProperty.Create(
            nameof(NumberOfFields),
            typeof(int),
            typeof(UI.Controls.StepProgressBarControl),
            0,
            propertyChanged: OnNumberOfFieldsPropertyChanged);

        public static readonly BindableProperty ProgressPercentProperty = BindableProperty.Create(
            nameof(ProgressPercent),
            typeof(int),
            typeof(UI.Controls.StepProgressBarControl),
            0,
            propertyChanged: OnProgressPercentPropertyChanged);

        public static readonly BindableProperty StatusProgressProperty = BindableProperty.Create(
            nameof(StatusProgress),
            typeof(ProgressState),
            typeof(UI.Controls.StepProgressBarControl),
            ProgressState.None,
            propertyChanged: OnStatusProgressPropertyChanged);

        public int NumberOfFields
        {
            get => (int) GetValue(NumberOfFieldsProperty);
            set => SetValue(NumberOfFieldsProperty, value);
        }

        public int ProgressPercent
        {
            get => (int) GetValue(ProgressPercentProperty);
            set => SetValue(ProgressPercentProperty, value);
        }

        public ProgressState StatusProgress
        {
            get => (ProgressState) GetValue(StatusProgressProperty);
            set => SetValue(StatusProgressProperty, value);
        }

        private static void OnNumberOfFieldsPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            if (bindable is UI.Controls.StepProgressBarControl progressBar && newvalue is int newNumber)
            {
                progressBar.UpdateLayoutFields(newNumber);
            }
        }

        private static void OnProgressPercentPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            if (bindable is UI.Controls.StepProgressBarControl progressBar && newvalue is int newNumber)
            {
                progressBar.UpdateStatesBarFields(newNumber, progressBar.StatusProgress);
            }
        }

        private static void OnStatusProgressPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            if (bindable is UI.Controls.StepProgressBarControl progressBar && newvalue is ProgressState newNumber)
            {
                progressBar.UpdateStatesBarFields(progressBar.ProgressPercent, newNumber);
            }
        }
    }
}
