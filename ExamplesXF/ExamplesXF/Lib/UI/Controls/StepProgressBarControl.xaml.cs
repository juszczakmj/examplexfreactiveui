using System.Collections.Generic;
using ExamplesXF.Lib.Enums;
using ExamplesXF.Lib.UI.Utils.Progress;
using Xamarin.Forms;

namespace ExamplesXF.Lib.UI.Controls
{
    public partial class StepProgressBarControl
    {
        private const double FieldsHorizontalSpace = 2;
        private Dictionary<StepProgress, ProgressBarElement>? _dictionaryFields;

        public StepProgressBarControl()
        {
            InitializeComponent();
        }

        private void UpdateLayoutFields(int numberFields)
        {
            _dictionaryFields = new Dictionary<StepProgress, ProgressBarElement>();

            var fieldWith = CalculateFieldWidth(numberFields);
            var interval = 100 / numberFields;
            var updatedStackLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal, 
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Spacing = 2
            };

            for (int i = 0; i < numberFields; i++)
            {
                var barField = new ProgressBarElement() 
                { 
                    WidthRequest = fieldWith, 
                    Status = ProgressState.None
                };

                var initial = i * interval + 1;
                _dictionaryFields.Add(new StepProgress(initial, (i + 1) * interval), barField);

                updatedStackLayout.Children.Add(barField);
                FrameControl.Content = updatedStackLayout;
            }
        }

        private void UpdateStatesBarFields(int percent, ProgressState status)
        {
            if (_dictionaryFields != null)
            {
                foreach (var field in _dictionaryFields)
                {
                    if (percent < field.Key.Initial)
                    {
                        field.Value.Status = ProgressState.None;
                    }
                    else if (percent > field.Key.Limit)
                    {
                        field.Value.Status = ProgressState.Success;
                    }
                    else if (percent >= field.Key.Initial && percent <= field.Key.Limit)
                    {
                        field.Value.Status = status == ProgressState.Failed ? ProgressState.Failed : ProgressState.Active;
                    }
                }
            }
        }
        
        private double CalculateFieldWidth(int number)
        {
            var horizontalMargin = 24;
            return (Application.Current.MainPage.Width - 2 * horizontalMargin - (number - 1) * FieldsHorizontalSpace) / number;
        }
    }
}
