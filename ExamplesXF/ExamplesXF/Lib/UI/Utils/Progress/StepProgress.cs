using System;

namespace ExamplesXF.Lib.UI.Utils.Progress
{
    public class StepProgress
    {
        public int Initial { get; }
        public int Limit { get; }

        public StepProgress(int initial, int limit)
        {
            Initial = initial;
            Limit = limit;

            if (initial > limit)
            {
                throw new ArgumentException("Initial step can not be smaller than step limit");
            }
        }
    }
}
