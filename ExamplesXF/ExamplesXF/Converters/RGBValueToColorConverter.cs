using System;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExamplesXF.Converters
{
    public class RgbValueToColorConverter : IMultiValueConverter, IMarkupExtension
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var defaultColor = Color.FromRgb(0,0,0);

            return values.Any(x => x == null) ? defaultColor : Color.FromRgb((int)values[0], (int)values[1], (int)values[2]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            => throw new NotImplementedException();

        public object ProvideValue(IServiceProvider serviceProvider)
            => this;
    }
}