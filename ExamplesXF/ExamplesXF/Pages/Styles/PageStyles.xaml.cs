using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExamplesXF.Pages.Styles
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageStyles : ContentPage
    {
        public PageStyles()
        {
            InitializeComponent();
        }
    }
}