using System;
using System.Threading.Tasks;
using ExamplesXF.Pages.Styles;
using ExamplesXF.ViewModels;
using Xamarin.Forms.Xaml;

namespace ExamplesXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : PageStyles
    {
        public MenuPage()
        {
            InitializeComponent();
        }
        
        async void MultiConverterButtonOnClicked(object sender, EventArgs e)
        {
            var page = new MultiConverterPage();
            await Navigation.PushAsync(page);
        }
        
        async void ReactiveFirstSampleButtonOnClicked(object sender, EventArgs e)
        {
            var page = new ReactiveFirstSamplePage();
            await Navigation.PushAsync(page);
        }
        
        async void ReactiveHandlerSampleButtonOnClicked(object sender, EventArgs e)
        {
            var page = new ReactiveEventHandlerSamplePage(new ReactiveEventHandlerSampleViewModel());
            await Navigation.PushAsync(page);
        }

        async void OneProgressBarElementClicked(object sender, EventArgs e)
        {
            var page = new ProgressBarElementPage();
            await Navigation.PushAsync(page);
        }

        async void StepProgressBarButtonClicked(object sender, EventArgs e)
        {
            var page = new StepProgressBarPage();
            await Navigation.PushAsync(page);
        }
    }
}