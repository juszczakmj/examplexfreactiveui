using Xamarin.Forms.Xaml;

namespace ExamplesXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProgressBarElementPage
    {
        public ProgressBarElementPage()
        {
            InitializeComponent();
        }
    }
}

