using Xamarin.Forms.Xaml;

namespace ExamplesXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReactiveFirstSamplePage
    {
        public ReactiveFirstSamplePage()
        {
            InitializeComponent();
        }
    }
}