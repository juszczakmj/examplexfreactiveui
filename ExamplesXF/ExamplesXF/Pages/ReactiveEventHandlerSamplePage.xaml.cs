using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using ExamplesXF.ViewModels;
using ReactiveUI;
using Xamarin.Forms.Xaml;

namespace ExamplesXF.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReactiveEventHandlerSamplePage
    {
        public ReactiveEventHandlerSamplePage(ReactiveEventHandlerSampleViewModel viewModel)
        {
            InitializeComponent();

            viewModel.TimeAlertObservable
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(x=> ShowAlertAsync(x));
        }

        private async Task ShowAlertAsync(TimeSpan time)
        {
            var msg = $"Next alert in {time.Seconds}.{time.Milliseconds}";
            await DisplayAlert("Random Alert!!!", msg,"Press...");
        }
    }
}