

# Simply application in Xamarin Forms

> **This application demonstrates:**

- Simply project of **XF**
- How to create **Bindings**
- How to use **Navigaiton**
- How to use **Dark**/**Light** theme
- **Examples** of use of some functionalities

> **Options actualy implemented:**

1. How to create **MultiValueConverter**
2. How to use **ReactiveUI**
3. How to use **Event Handlers**
4. Custom UI controls **Progress Bar Sample**


